using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Ocelot.Configuration.Creator;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using System;
using Travlr.Shared.Application.Contexts;
using Travlr.Shared.Application.ExternalServices;
using Travlr.Shared.Application.ExternalServices.SSO;
using Travlr.Shared.Application.Options;
using Travlr.Shared.Application.UseCases;
using Travlr.Shared.Application.UseCases.Authentications.ValidateAuthenticationToken;
using Travlr.Shared.Core.Enums;
using Travlr.Shared.Infrastructure.AppConfigs;
using Travlr.Shared.Infrastructure.ExternalServices;
using Travlr.Shared.Infrastructure.ExternalServices.SSO;
using Travlr.Shared.Web;
using Travlr.Shared.Web.Authentications;
using Travlr.Shared.Web.Options;

namespace Travlr.ApiGateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Platform = Configuration.GetSection("Platform").Get<Platform?>();
        }

        public IConfiguration Configuration { get; }
        public Platform? Platform { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOcelot();
            services.AddTransient<UseCase>();
            services.AddTransient<IPlatformOptions<ServiceOptions>>(serviceProvider => new DummyPlatformOptions<ServiceOptions>(Configuration.GetSection("ServiceOptions").Get<ServiceOptions>()));
            services.AddTransient<IWebserviceAPI, WebServiceAPI>();
            services.AddTransient<ISsoService, SsoService>();
            services.AddTransient<IUseCaseHandler<ValidateAuthenticationTokenInput, ValidateAuthenticationTokenOutput>, ValidateAuthenticationTokenUseCase>();
            services.AddAuthentication().AddScheme<SsoCookieAuthenticationOptions, SsoCookieAuthenticationHandler>("SsoCookie", (options) => { });
            services.AddSingleton(StartupExtensions.AddImagery);
            services.AddTransient(ResolveSiteContext);
        }

        private ISiteContext ResolveSiteContext(IServiceProvider serviceProvider)
        {
            var platform = Configuration.GetSection("Platform").Get<Platform?>();
            if (platform.HasValue)
            {
                return null;
            }

            var host = serviceProvider.GetService<IHttpContextAccessor>().HttpContext.Request.Host;

            return new SiteContext { Hostname = host.Value, Platform = platform.Value };
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var configuration = new OcelotPipelineConfiguration
            {
                PreQueryStringBuilderMiddleware = async (ctx, next) =>
                {
                    var host = ctx.HttpContext.Request.Host;
                    var originalHostQueryString = $"host={host.Value}";

                    if (string.IsNullOrEmpty(ctx.DownstreamRequest.Query))
                    {
                        ctx.DownstreamRequest.Query = originalHostQueryString;
                    }
                    else
                    {
                        ctx.DownstreamRequest.Query = $"{ctx.DownstreamRequest.Query}&{originalHostQueryString}";
                    }

                    if (Platform.HasValue)
                    {
                        ctx.DownstreamRequest.Query = $"{ctx.DownstreamRequest.Query}&platform={Platform.Value}";
                    }

                    await next.Invoke();
                }
            };

            app.UseRouting();
            app.UseOcelot(configuration);
            app.UseAuthentication();
            app.UseEndpoints(endpoints => { });
        }
    }
}
