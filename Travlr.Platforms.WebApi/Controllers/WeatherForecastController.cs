﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Travlr.Shared.Application.Contexts;
using Travlr.Shared.Application.UseCases;
using Travlr.Shared.Application.UseCases.Options.GetOptions;

namespace Travlr.Platforms.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly CachedUseCase _useCase;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, CachedUseCase useCase, ISiteContext siteContext)
        {
            _logger = logger;
            _useCase = useCase;
        }

        [HttpGet]
        public async Task<IEnumerable<WeatherForecast>> Get(int ssoId, string host, int anu)
        {
            var x = await _useCase.Execute(new GetOptionsInput { Platform = Shared.Core.Enums.Platform.Bali });
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
