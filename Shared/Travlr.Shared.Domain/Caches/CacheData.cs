﻿using System;
using Travlr.Shared.Core.Utilities;

namespace Travlr.Shared.Domain.Caches
{
    public class CacheData
    {
        public CacheData(string className, string inputJson, string outputJson, DateTime executedUtc, DateTime expiryUtc)
        {
            HashCode = ConstructHashCode(className, inputJson);
            ClassName = className;
            InputJson = inputJson;
            OutputJson = outputJson;
            ExecutedUtc = executedUtc;
            ExpiryUtc = expiryUtc;
        }

        public CacheData(int hashCode, string className, string inputJson, string outputJson, DateTime executedUtc, DateTime expiryUtc)
        {
            HashCode = hashCode;
            ClassName = className;
            InputJson = inputJson;
            OutputJson = outputJson;
            ExecutedUtc = executedUtc;
            ExpiryUtc = expiryUtc;
        }

        public TOutput GetOutput<TOutput>()
        {
            return OutputJson.Deserialize<TOutput>();
        }

        public void SetExpiryUtc(DateTime expiryUtc)
        {
            ExpiryUtc = expiryUtc;
        }

        public bool IsDerivedFrom<TInput, TOutput>(ICacheable<TInput, TOutput> obj, TInput input) where TInput : ICachedInput<TOutput>
        {
            return ClassName == GetClassName(obj) && InputJson.Equals(input.Serialize());
        }

        public static CacheData Create<TInput, TOutput>(ICacheable<TInput, TOutput> obj, TInput input, TOutput output) where TInput : ICachedInput<TOutput>
        {
            var executedUtc = DateTime.UtcNow;

            // Store expiry time before execution by 1 second so periodic cache priming will work every time
            var expiryUtc = executedUtc.AddSeconds(obj.CacheInSeconds - 1);

            return new CacheData(GetClassName(obj), input.Serialize(), output.Serialize(), executedUtc, expiryUtc);
        }

        public static int ConstructHashCode<TInput, TOutput>(ICacheable<TInput, TOutput> obj, TInput input) where TInput : ICachedInput<TOutput>
        {
            return ConstructHashCode(obj.GetType().Name, input.Serialize());
        }

        public static string ConstructKey<TInput, TOutput>(ICacheable<TInput, TOutput> obj, TInput input) where TInput : ICachedInput<TOutput>
        {
            return $"{obj.GetType().Name}-{input.Serialize()}";
        }

        private static int ConstructHashCode(string className, string inputJson)
        {
            return $"{className}{inputJson}".GetStableHashCode();
        }

        private static string GetClassName<TInput, TOutput>(ICacheable<TInput, TOutput> obj) where TInput : ICachedInput<TOutput>
        {
            return obj.GetType().Name;
        }

        public int HashCode { get; private set; }
        public DateTime ExecutedUtc { get; private set; }
        public DateTime ExpiryUtc { get; private set; }
        public string ClassName { get; private set; }
        public string InputJson { get; private set; }
        public string OutputJson { get; private set; }
    }
}
