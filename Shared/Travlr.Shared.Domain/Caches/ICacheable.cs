﻿namespace Travlr.Shared.Domain.Caches
{
    public interface ICacheable<TInput, TOutput> where TInput : ICachedInput<TOutput>
    {
        int CacheInSeconds { get; }
    }
}
