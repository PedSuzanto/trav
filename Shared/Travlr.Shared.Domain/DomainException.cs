﻿using System;

namespace Travlr.Shared.Domain
{
    public class DomainException : Exception
    {
        public DomainException(string businessMessage)
            : base(businessMessage)
        {
        }
    }
}
