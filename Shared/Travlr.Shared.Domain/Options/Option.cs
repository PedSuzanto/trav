﻿using System;
using Travlr.Shared.Core.Utilities;

namespace Travlr.Shared.Domain.Options
{
    public class Option
    {
        public int Id { get; private set; }

        public string Name { get; private set; }

        public string Value { get; private set; }

        public DateTime? CreatedAt { get; private set; }

        public DateTime? UpdatedAt { get; private set; }

        public bool IsPlatformOption
        {
            get
            {
                return Name.Equals(Core.Constants.Options.PlatformIdentifier, StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public bool IsWhiteLabelProductOption
        {
            get
            {
                return Name.StartsWith(Core.Constants.Options.WlpIdentifier, StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public Option(int id, string name, string value, DateTime? createdAt, DateTime? updatedAt)
        {
            Id = id;
            Name = name;
            Value = value;
            CreatedAt = createdAt;
            UpdatedAt = updatedAt;
        }

        internal static Option Create(string name, string value)
        {
            var now = DateTime.Now;

            return new Option(0, name, value, now, now);
        }

        internal static Option Create<TClass>(string name, TClass value) where TClass : class
        {
            return Create(name, value.Serialize());
        }

        public void SetValue(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new DomainException("Value cannot be empty.");
            }

            Value = value;
        }

        public void SetValue<TClass>(TClass value) where TClass : class
        {
            SetValue(value.Serialize());
        }

        public TClass GetValue<TClass>() where TClass : class
        {
            return Value.Deserialize<TClass>();
        }
    }
}
