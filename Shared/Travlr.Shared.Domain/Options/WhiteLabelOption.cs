﻿using System.Collections.Generic;

namespace Travlr.Shared.Domain.Options
{
    public class WhiteLabelOption
    {
        public string Contact { get; set; }
        public Home Home { get; set; }
        public Map Map { get; set; }
        public string Domain { get; set; }
        public SiteOption RegionalOptions { get; set; }
    }

    public class Map
    {
        public List<Location> Locations { get; set; }
    }

    public class Location
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Marker { get; set; }
        public string Name { get; set; }
    }

    public class Home
    {
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string Description { get; set; }
        public string Footer { get; set; }
        public List<Content> Content { get; set; }
        public List<Content> Slide { get; set; }
    }

    public class Content
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string Snippet { get; set; }
        public string Thumb { get; set; }
    }
}