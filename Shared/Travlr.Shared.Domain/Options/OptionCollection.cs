﻿using System.Collections.Generic;
using System.Linq;

namespace Travlr.Shared.Domain.Options
{
    public class OptionCollection
    {
        public IList<Option> Options { get; }
        public OptionCollection(IList<Option> options)
        {
            Options = options;
        }

        public Option PlatformOption => Options.Single(x => x.IsPlatformOption);

        public ICollection<Option> WhiteLabelOptions => Options.Where(x => x.IsWhiteLabelProductOption).ToList();

        public ICollection<Option> OtherOptions => Options.Where(x => !x.IsPlatformOption && !x.IsWhiteLabelProductOption).ToList();
        
        public void Add(string name, string value)
        {
            ValidateNameExists(name);

            var option = Option.Create(name, value);

            Options.Add(option);
        }

        public void Add<TClass>(string name, TClass value) where TClass : class
        {
            ValidateNameExists(name);

            var option = Option.Create(name, value);

            Options.Add(option);
        }

        private void ValidateNameExists(string name)
        {
            if (Options.Any(x => x.Name.Equals(name)))
            {
                throw new DomainException("Cannot add option with same name.");
            }
        }
    }
}
