﻿using System;
using System.Collections.Generic;

namespace Travlr.Shared.Domain.Options
{
    public class SiteOption
    {
        public int Id { get; set; }
        public string AppTheme { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Favicon { get; set; }
        public string Domain { get; set; }
        public string FooterCopy { get; set; }
        public LogoOption Logo { get; set; }
        public string Brand { get; set; }
        public List<string> Destinations { get; set; }
        public List<LinkOption> SocialMedia { get; set; }
        public List<LinkOption> Permalink { get; set; }
        public Meta DefaultMeta { get; set; }
        public string LogUrl { get; set; }
        public CDNOption cdnOption { get; set; }
        public SSOOption ssoOption { get; set; }
        public IntercomOption Intercom { get; set; }
        public CampaignMonitor CampaignMonitor { get; set; }
        public List<string> AvailableCurrencies { get; set; }
        public Homepage Homepage { get; set; }
        public Hummpage Hummpage { get; set; }
        public InstagramOptions InstagramOptions { get; set; }
        public FederatedSearchIcon SearchIcon { get; set; }
        public bool IsWLP { get; set; } = false;
        public PaymentMethodsOption PaymentMethods { get; set; }
        public string SupportMail { get; set; }

        public string BrandUrl { get; set; }

        public Dictionary<string, Dictionary<string, string>> ThemeColorDictionary { get; set; }

        public List<HeaderMenuItem> HeaderMenu { get; set; } = new List<HeaderMenuItem>();
        public Guid RefId { get; set; }
        public string GTMId { get; set; }
    }

    public class HeaderMenuItem
    {
        public string Text { get; set; }
        public string Link { get; set; }
        public string Label { get; set; }
        public string LabelColor { get; set; }
        public List<HeaderMenuItem> SubMenus { get; set; }
    }

    public class PaymentMethodsOption
    {
        public PaymentMethodHummOption Humm { get; set; }
    }

    public class PaymentMethodHummOption
    {
        public bool Enabled { get; set; } = false;
    }

    public class CampaignMonitor
    {
        public string Logo { get; set; }
        public Link Links { get; set; }
        public List<EmailTemplate> EmailTemplates { get; set; }
        public List<string> BccAddresses { get; set; }
    }

    public class Link
    {
        public string SignIn { get; set; }
        public string SignUp { get; set; }
        public string Faqs { get; set; }
        public string Unsubscribe { get; set; }
        public string CustomerService { get; set; }
        public string MyBookingLink { get; set; }
    }

    public class CDNOption
    {
        public string Bucket { get; set; }

        public string CDN { get; set; }
    }

    public class LinkOption
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
    }

    public class EmailTemplate
    {
        public string Name { get; set; }
        public string TemplateId { get; set; }

        public override bool Equals(object obj)
        {
            var emailTemplate = (EmailTemplate)obj;

            return TemplateId.Equals(emailTemplate.TemplateId, StringComparison.InvariantCultureIgnoreCase);
        }

        public override int GetHashCode()
        {
            return TemplateId.GetHashCode();
        }
    }

    public class Meta
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Site { get; set; }
    }

    public class LogoOption
    {
        public string Dark { get; set; }
        public string White { get; set; }
        public string Alt { get; set; }
    }

    public class SSOOption
    {
        public Boolean disableSocialLogin { get; set; }
        public Boolean enableAdditionalUserRestriction { get; set; }
        public string userRestrictionMatcher { get; set; }
        public List<string> whitelistedPaths { get; set; }
        public string caveats { get; set; }
        public string emailPlaceholder { get; set; }
    }

    public class IntercomOption
    {
        public string Id { get; set; }
        public string Secret { get; set; }
    }

    public class Homepage
    {
        public List<DynamicSection> Sections { get; set; }
        public List<string> BeforeLogin { get; set; }
        public List<string> AfterLogin { get; set; }
        public StaticSection BannerHeader { get; set; }
        public StaticSection BannerPromo { get; set; }
        public StaticSection BannerCampaign { get; set; }
        public StaticSection FindInspiration { get; set; }
        public StaticSection Holiday { get; set; }
        public StaticSection Flight { get; set; }
        public List<ShortcutIcon> ShortcutPanel { get; set; }
        public int SpecialDeal { get; set; }
        public StaticSection TrendingDestinations { get; set; }
        public StaticSection SliderTiles { get; set; }
        public StaticSection OurApps { get; set; }
        public StaticSection MediaPartner { get; set; }
    }

    public class Hummpage
    {
        public List<LinkOption> CallToAction { get; set; }

        public List<DynamicSection> Sections { get; set; }
    }

    public class BaseSection
    {
        public string Name { get; set; }
        public string Header { get; set; }
        public string SubHeader { get; set; }
        public string Image { get; set; }
        public SectionButton SectionButton { get; set; }
    }

    public class DynamicSection : BaseSection
    {
        public List<int> Ids { get; set; }
    }

    public class StaticSection : BaseSection
    {
        public List<SectionData> Data { get; set; }

    }

    public class SectionButton
    {
        public string Title { get; set; }
        public string Url { get; set; }
    }

    public class ShortcutIcon
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
    }

    public class SectionData
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public List<Attribute> Attributes { get; set; }
    }

    public class Attribute
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class InstagramOptions
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string ProfilePicture { get; set; }
        public int Media { get; set; }
        public int Follows { get; set; }
        public int FollowedBy { get; set; }
    }

    public class FederatedSearchIcon
    {
        public string Destination { get; set; }
        public string Shortlist { get; set; }
        public string Event { get; set; }
        public string Trip { get; set; }
        public string Article { get; set; }
        public string Listing { get; set; }
    }
}