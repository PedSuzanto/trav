﻿using Travlr.Shared.Core.Enums;

namespace Travlr.Shared.Application.Contexts
{
    public interface ISiteContext
    {
        Platform Platform { get; set; }

        string Hostname { get; set; }
    }
}
