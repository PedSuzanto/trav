﻿using Travlr.Shared.Core.Enums;

namespace Travlr.Shared.Application.Contexts
{
    public class SiteContext : ISiteContext
    {
        public Platform Platform { get; set; }

        public string Hostname { get; set; }

        public string SiteName { get; set; }
    }
}
