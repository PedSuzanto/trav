﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Travlr.Shared.Application.DependencyInjections
{
    public interface IServiceConfiguration
    {
        void ConfigureServices(IServiceCollection services, IConfiguration config);
    }
}
