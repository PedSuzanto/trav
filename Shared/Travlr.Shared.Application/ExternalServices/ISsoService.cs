﻿using System.Threading.Tasks;
using Travlr.Shared.Application.UseCases.Authentications.ValidateAuthenticationToken;

namespace Travlr.Shared.Application.ExternalServices.SSO
{
    public interface ISsoService
    {
        Task<ValidateAuthenticationTokenOutput> ValidateToken(string token);
    }
}