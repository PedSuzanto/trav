﻿using System.Threading.Tasks;

namespace Travlr.Shared.Application.ExternalServices
{
    public interface IWebserviceAPI
    {
        Task<TReturn> Get<TReturn>(string url, object request) where TReturn : class;

        Task<TReturn> Post<TReturn>(string url, object request, string mediaType = "application/json") where TReturn : class;

        Task<TReturn> Put<TReturn>(string url, object request) where TReturn : class;
    }
}
