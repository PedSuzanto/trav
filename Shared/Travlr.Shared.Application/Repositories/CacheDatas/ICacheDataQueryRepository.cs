﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Travlr.Shared.Domain.Caches;

namespace Travlr.Shared.Application.Repositories.CacheDatas
{
    public interface ICacheDataQueryRepository
    {
        Task<IEnumerable<string>> GetAllCacheDataClassNames();
        Task<CacheData> GetCacheData(int hashCode, DateTime executedUtc);
        Task<IEnumerable<CacheData>> SearchCachedData(string className, string keyword);
    }
}