﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Travlr.Shared.Domain.Caches;

namespace Travlr.Shared.Application.Repositories.CacheDatas
{
    public interface ICacheDataCommandRepository
    {
        Task ClearCacheData(DateTime executedUtc);
        Task ExpireCaches(IEnumerable<int> hashCodes);
        Task PutCacheData(CacheData cacheData);
    }
}