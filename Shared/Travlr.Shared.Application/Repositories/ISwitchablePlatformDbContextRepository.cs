﻿using Travlr.Shared.Core.Enums;

namespace Travlr.Shared.Application.Repositories
{
    public interface ISwitchablePlatformDbContextRepository
    {
        void SwitchPlatformDbContext(Platform platform);
    }
}
