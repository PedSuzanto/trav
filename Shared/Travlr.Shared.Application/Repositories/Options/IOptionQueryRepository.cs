﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Travlr.Shared.Domain.Options;

namespace Travlr.Shared.Application.Repositories.Options
{
    public interface IOptionQueryRepository : ISwitchablePlatformDbContextRepository
    {
        Task<IList<Option>> GetOptions();
        Task<Option> GetOption(string key);
    }
}
