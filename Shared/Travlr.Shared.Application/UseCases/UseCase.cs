﻿using System;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace Travlr.Shared.Application.UseCases
{
    public class UseCase : CachedUseCase
    {
        public UseCase(IServiceProvider serviceProvider)
            :base(serviceProvider)
        {
        }

        public async Task Execute<TInput>(TInput input) where TInput : IUseCaseInput
        {
            var service = _serviceProvider.GetService<IUseCaseHandler<TInput>>();

            await service.Execute(input);
        }

        public new async Task<TOutput> Execute<TOutput>(IUseCaseInput<TOutput> input)
        {
            var inputType = input.GetType();
            var handler = GetHandler<TOutput>(inputType);

            return await handler.Handle(_serviceProvider, input);
        }
    }
}
