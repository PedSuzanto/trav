﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Travlr.Shared.Application.Contexts;
using Travlr.Shared.Application.UseCases.Options.GetOptions;
using Travlr.Shared.Core.Enums;

namespace Travlr.Shared.Application.UseCases.Options.GetDomainPlatformMap
{
    public class GetDomainPlatformMapInput : IUseCaseInput<IDictionary<string, SiteContext>> { }

    public class GetDomainPlatformMapUseCase : IUseCaseHandler<GetDomainPlatformMapInput, IDictionary<string, SiteContext>>
    {
        private readonly CachedUseCase _useCase;

        public GetDomainPlatformMapUseCase(CachedUseCase useCase)
        {
            _useCase = useCase;
        }

        public async Task<IDictionary<string, SiteContext>> Execute(GetDomainPlatformMapInput input)
        {
            var result = new Dictionary<string, SiteContext>();
            var getOptionsInput = new GetOptionsInput();

            foreach (var platform in (Platform[])Enum.GetValues(typeof(Platform)))
            {
                getOptionsInput.Platform = platform;
                var options = await _useCase.Execute(getOptionsInput);

                foreach (var option in options.WhiteLabelOptions)
                {
                    var siteOption = option.GetValue<SimpleSiteOption>();
                    var host = siteOption.Domain;

                    result.TryAdd(host, new SiteContext { Hostname = host, Platform = platform, SiteName = siteOption.SiteName });
                }
            }

            return result;
        }
    }
}
