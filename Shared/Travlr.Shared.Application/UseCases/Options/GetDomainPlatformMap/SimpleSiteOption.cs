﻿namespace Travlr.Shared.Application.UseCases.Options.GetDomainPlatformMap
{
    internal class SimpleSiteOption
    {
        public string Domain { get; set; }

        public SimpleWlpOption RegionalOptions { get; set; }

        public string SiteName
        {
            get
            {
                return RegionalOptions.Name;
            }
        }
    }

    internal class SimpleWlpOption
    {
        public string Name { get; set; }
    }
}
