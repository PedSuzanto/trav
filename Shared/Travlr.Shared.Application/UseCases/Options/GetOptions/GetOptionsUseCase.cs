﻿using System.Threading.Tasks;
using Travlr.Shared.Application.Repositories.Options;
using Travlr.Shared.Core.Constants;
using Travlr.Shared.Core.Enums;
using Travlr.Shared.Domain.Options;

namespace Travlr.Shared.Application.UseCases.Options.GetOptions
{
    public class GetOptionsInput : IUseCaseInput<OptionCollection>
    {
        public Platform? Platform { get; set; }
    }

    public class GetOptionsUseCase : ICacheableUseCaseHandler<GetOptionsInput, OptionCollection>
    {
        private readonly IOptionQueryRepository _repository;

        public GetOptionsUseCase(IOptionQueryRepository repository)
        {
            _repository = repository;
        }

        public int CacheInSeconds => DurationInSeconds.Duration5Mins;

        public async Task<OptionCollection> Execute(GetOptionsInput input)
        {
            if (input.Platform.HasValue)
            {
                _repository.SwitchPlatformDbContext(input.Platform.Value);
            }

            var options = await _repository.GetOptions();

            return new OptionCollection(options);
        }

        public Task<OptionCollection> Execute(IUseCaseInput<OptionCollection> input)
        {
            throw new System.NotImplementedException();
        }
    }
}
