﻿using System.Threading.Tasks;
using Travlr.Shared.Application.ExternalServices.SSO;

namespace Travlr.Shared.Application.UseCases.Authentications.ValidateAuthenticationToken
{
    public class ValidateAuthenticationTokenInput : IUseCaseInput<ValidateAuthenticationTokenOutput>
    {
        public string Token { get; set; }
    }

    public class ValidateAuthenticationTokenUseCase : IUseCaseHandler<ValidateAuthenticationTokenInput, ValidateAuthenticationTokenOutput>
    {
        private readonly ISsoService _ssoService;

        public ValidateAuthenticationTokenUseCase(ISsoService ssoService)
        {
            _ssoService = ssoService;
        }

        public async Task<ValidateAuthenticationTokenOutput> Execute(ValidateAuthenticationTokenInput input)
        {
            var result = await _ssoService.ValidateToken(input.Token);

            return result;
        }
    }
}
