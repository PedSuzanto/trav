﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travlr.Shared.Application.UseCases.Authentications.ValidateAuthenticationToken
{
    public class ValidateAuthenticationTokenOutput
    {
        public string message { get; set; }
        public UserResponseData data { get; set; }
    }

    public class UserResponseData
    {
        public UserData user { get; set; }
    }

    public class UserData
    {
        public int id { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string avatar { get; set; }
        public string facebookId { get; set; }
        public string instagramId { get; set; }
        public string twitterId { get; set; }
        public string googleId { get; set; }
        public string oauthId { get; set; }
        public string oauthProvider { get; set; }
        public string address { get; set; }
    }
}
