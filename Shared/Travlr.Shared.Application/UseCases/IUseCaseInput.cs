﻿using Travlr.Shared.Domain.Caches;

namespace Travlr.Shared.Application.UseCases
{
    public interface IUseCaseInput
    {
    }

    public interface IUseCaseInput<TOutput> : ICachedInput<TOutput>
    {
    }
}
