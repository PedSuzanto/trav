﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Travlr.Shared.Domain.Caches;

namespace Travlr.Shared.Application.UseCases
{
    public interface IUseCaseHandler<TInput, TOutput> where TInput : IUseCaseInput<TOutput>
    {
        Task<TOutput> Execute(TInput input);
    }
    public interface IUseCaseHandlerWrapper<TOutput>
    {
        Task<TOutput> Execute(IUseCaseInput<TOutput> input);
    }

    public interface ICacheableUseCaseHandler<TInput, TOutput> : IUseCaseHandler<TInput, TOutput>, ICacheable<TInput, TOutput> where TInput : IUseCaseInput<TOutput>
    {
    }

    public interface IUseCaseHandler<TInput> where TInput : IUseCaseInput
    {
        Task Execute(TInput input);
    }
}
