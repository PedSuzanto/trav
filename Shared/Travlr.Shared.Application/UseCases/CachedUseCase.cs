﻿using System;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Travlr.Shared.Application.Repositories.CacheDatas;
using Travlr.Shared.Domain.Caches;
using System.Collections.Concurrent;
using Microsoft.Extensions.Caching.Distributed;
using Travlr.Shared.Core.Utilities;
using Travlr.Shared.Application.Repositories.Options;

namespace Travlr.Shared.Application.UseCases
{
    public class CachedUseCase
    {
        protected readonly IServiceProvider _serviceProvider;
        protected static readonly ConcurrentDictionary<Type, object> _requestHandlers = new ConcurrentDictionary<Type, object>();

        public CachedUseCase(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public virtual async Task<TOutput> Execute<TOutput>(IUseCaseInput<TOutput> input)
        {
            var inputType = input.GetType();
            var handler = GetHandler<TOutput>(inputType);

            return await handler.HandleCached(_serviceProvider, input);
        }

        internal static UseCaseHandlerWrapper<TOutput> GetHandler<TOutput>(Type requestType)
        {
            return (UseCaseHandlerWrapper<TOutput>)_requestHandlers.GetOrAdd(requestType,
                            t => Activator.CreateInstance(typeof(UseCaseHandlerWrapperImpl<,>).MakeGenericType(requestType, typeof(TOutput))));
        }
    }

    internal abstract class UseCaseHandlerWrapper<TOutput>
    {
        public abstract Task<TOutput> Handle(IServiceProvider serviceProvider, IUseCaseInput<TOutput> input);

        public abstract Task<TOutput> HandleCached(IServiceProvider serviceProvider, IUseCaseInput<TOutput> input);
    }

    internal class UseCaseHandlerWrapperImpl<TInput, TOutput> : UseCaseHandlerWrapper<TOutput>
        where TInput : IUseCaseInput<TOutput>
    {
        public override async Task<TOutput> Handle(IServiceProvider serviceProvider, IUseCaseInput<TOutput> input)
        {
            var service = serviceProvider.GetService<IUseCaseHandler<TInput, TOutput>>();

            return await service.Execute((TInput)input);
        }

        public override async Task<TOutput> HandleCached(IServiceProvider serviceProvider, IUseCaseInput<TOutput> input)
        {
            var useCase = serviceProvider.GetService<ICacheableUseCaseHandler<TInput, TOutput>>();
            var castedInput = (TInput)input;
            if (useCase == null || useCase.CacheInSeconds < 1)
            {
                return await useCase.Execute(castedInput);
            }

            var _optionReadOnlyRepository = serviceProvider.GetService<IOptionQueryRepository>();
            var cacheFeature = await _optionReadOnlyRepository.GetOption(Core.Constants.Options.CacheFeatureOptionsKey);

            if (cacheFeature != null && cacheFeature.Value == Core.Constants.Options.CacheFeatureMySql)
                return await ExecuteCachedMySql(serviceProvider, useCase, castedInput);
            else if (cacheFeature != null && cacheFeature.Value == Core.Constants.Options.CacheFeatureRedis)
                return await ExecuteDistributedCached(serviceProvider, useCase, castedInput);
            else
                return await useCase.Execute(castedInput);
        }

        private async Task<TOutput> ExecuteCachedMySql(IServiceProvider serviceProvider, ICacheableUseCaseHandler<TInput, TOutput> useCase, TInput input)
        {
            var executedUtc = DateTime.UtcNow;
            var hashCode = CacheData.ConstructHashCode(useCase, input);
            var queryRepository = serviceProvider.GetService<ICacheDataQueryRepository>();
            var cacheData = await queryRepository.GetCacheData(hashCode, executedUtc);

            // Check for hashCode collision and valid data
            if (cacheData != null && cacheData.IsDerivedFrom(useCase, input))
            {
                try
                {
                    var outputCached = cacheData.GetOutput<TOutput>();

                    return outputCached;
                }
                catch
                {
                    // If the JSON data is invalid for the given type then we just perform the use case as usual
                }
            }

            // Execute use case
            var output = await useCase.Execute(input);

            // Save usecase output to cache
            try
            {
                var commandRepository = serviceProvider.GetService<ICacheDataCommandRepository>();
                cacheData = CacheData.Create(useCase, input, output);

                await commandRepository.PutCacheData(cacheData);
            }
            catch
            {
                // If we can't store the cache data then we still want to go ahead with returning the live results
            }

            return output;
        }

        private async Task<TOutput> ExecuteDistributedCached(IServiceProvider serviceProvider, ICacheableUseCaseHandler<TInput, TOutput> useCase, TInput input)
        {
            var distributedCache = serviceProvider.GetService<IDistributedCache>();
            var key = CacheData.ConstructKey(useCase, input);

            try
            {
                var cacheData = await distributedCache.GetStringAsync(key);
                if (cacheData != null)
                {
                    var outputCached = cacheData.Deserialize<TOutput>();
                    return outputCached;
                }
            }
            catch
            {
                // If the JSON data is invalid for the given type then we just perform the use case as usual
            }

            // Execute use case
            var output = await useCase.Execute(input);

            // Save usecase output to cache
            try
            {
                var cacheData = CacheData.Create(useCase, input, output);
                var options = new DistributedCacheEntryOptions { AbsoluteExpiration = cacheData.ExpiryUtc };

                await distributedCache.SetStringAsync(key, cacheData.OutputJson, options);
            }
            catch
            {
                // If we can't store the cache data then we still want to go ahead with returning the live results
            }

            return output;
        }
    }
}
