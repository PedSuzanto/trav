﻿namespace Travlr.Shared.Application.Options
{
    public interface IPlatformOptions<T> where T : class
    {
        T Value { get; }
    }
}
