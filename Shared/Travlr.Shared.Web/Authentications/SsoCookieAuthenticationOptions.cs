﻿using Microsoft.AspNetCore.Authentication;

namespace Travlr.Shared.Web.Authentications
{
    public class SsoCookieAuthenticationOptions : AuthenticationSchemeOptions
    {
        public const string COOKIE_NAME = "travlr_sso_token";

        public SsoCookieAuthenticationOptions() : base()
        {
            ClaimsIssuer = "www.TRAVLR.com";
        }
    }
}
