﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Travlr.Shared.Application.UseCases;
using Travlr.Shared.Application.UseCases.Authentications.ValidateAuthenticationToken;
using Travlr.Shared.Core.Enums;
using Travlr.Shared.Core.Imagery;

namespace Travlr.Shared.Web.Authentications
{
    public static class MyClaimTypes
    {
        public static string Avatar = "https://schemas.travlr.com/identity/claims/avatar";
    }

    public class SsoCookieAuthenticationHandler : AuthenticationHandler<SsoCookieAuthenticationOptions>
    {
        private readonly ILogger _log;

        private readonly UseCase _useCase;

        private readonly Imagery _imagery;

        public SsoCookieAuthenticationHandler(
            IOptionsMonitor<SsoCookieAuthenticationOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            UseCase useCase,
            Imagery imagery)
            : base(options, logger, encoder, clock)
        {
            _log = logger.CreateLogger("SsoCookie");
            _useCase = useCase;
            _imagery = imagery;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Cookies.ContainsKey(SsoCookieAuthenticationOptions.COOKIE_NAME))
            {
                return AuthenticateResult.NoResult();
            }

            var token = Request.Cookies[SsoCookieAuthenticationOptions.COOKIE_NAME];

            if (string.IsNullOrEmpty(token))
            {
                return AuthenticateResult.NoResult();
            }

            _log.LogDebug("Got token from client: {token}", token);

            try
            {
                var result = await _useCase.Execute(new ValidateAuthenticationTokenInput { Token = token });
                var user = result?.data?.user;

                if (user == null)
                {
                    return AuthenticateResult.NoResult();
                }

                var claims = new List<Claim> {
                    new Claim(ClaimTypes.Hash, token),
                    // WARN: The ID below is an SSO ID
                    new Claim(ClaimTypes.Sid, user.id.ToString()),
                    new Claim(ClaimTypes.Email, user.email),
                    new Claim(ClaimTypes.Name, $"{user.firstName} {user.lastName}".Trim()),
                    new Claim(MyClaimTypes.Avatar, _imagery.CreateUrl(
                            user.avatar,
                            ImagerySize.Square,
                            _imagery.CreateLegacyUrl(user.avatar, ImageryType.Avatar)
                        )
                    ),
                };
                var identity = new ClaimsIdentity(claims, Scheme.Name);
                var principal = new ClaimsPrincipal(identity);
                var ticket = new AuthenticationTicket(principal, Scheme.Name);

                return AuthenticateResult.Success(ticket);
            }
            catch (HttpRequestException e)
            {
                _log.LogError($"Caught error when trying to validate 'Authentication Token': {e.Message}");

                return AuthenticateResult.NoResult();
            }
        }
    }
}
