﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Travlr.Shared.Application.Contexts;
using Travlr.Shared.Application.UseCases;
using Travlr.Shared.Application.UseCases.Options.GetDomainPlatformMap;
using Travlr.Shared.Core.Enums;

namespace Travlr.Shared.Web.Contexts
{
    public class SiteContextFactory
    {
        private const string HostKey = "host";
        private const string PlatformKey = "platform";
        private const string DefaultSiteName = "platform";
        private readonly UseCase _useCase;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private static ConcurrentDictionary<string, SiteContext> _siteContexts;

        public SiteContextFactory(UseCase useCase, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _useCase = useCase;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task RepopulateSiteContexts()
        {
            _siteContexts = new ConcurrentDictionary<string, SiteContext>();

            await PopulateSiteContexts();
        }

        public virtual async Task PopulateSiteContexts()
        {
            if (_siteContexts == null)
            {
                _siteContexts = new ConcurrentDictionary<string, SiteContext>();
            }

            PopulateFromConfiguration();

            await PopulateFromDatabase();
        }

        public virtual SiteContext GetInstance()
        {
            var host = GetHost();
            var platformQuery = GetPlatformFromQuery();
            if (platformQuery.HasValue)
            {
                return new SiteContext { Hostname = host, Platform = platformQuery.Value };
            }

            var platformConfig = _configuration.GetSection("Platform").Get<Platform?>();
            if (platformConfig.HasValue)
            {
                return new SiteContext { Hostname = host, Platform = platformConfig.Value, SiteName = DefaultSiteName };
            }

            if (_siteContexts != null && _siteContexts.ContainsKey(host))
            {
                return _siteContexts[host];
            }

            PopulateSiteContexts().Wait();

            if (_siteContexts.ContainsKey(host))
            {
                return _siteContexts[host];
            }

            return new SiteContext { Hostname = host, Platform = Platform.Bali, SiteName = DefaultSiteName };
        }

        private Platform? GetPlatformFromQuery()
        {
            var platformQuery = GetQueryStringValue(PlatformKey);

            if (string.IsNullOrEmpty(platformQuery))
            {
                return null;
            }
            
            return (Platform)Enum.Parse(typeof(Platform), platformQuery);
        }

        private string GetHost()
        {
            return GetQueryStringValue(HostKey);
        }

        private string GetQueryStringValue(string key)
        {
            if (_httpContextAccessor == null)
            {
                return string.Empty;
            }

            var request = _httpContextAccessor.HttpContext.Request;

            return request.Query.ContainsKey(key) ? request.Query[key].ToString() : string.Empty;
        }

        private async Task PopulateFromDatabase()
        {
            var domainPlatformMap = await _useCase.Execute(new GetDomainPlatformMapInput());

            foreach (var domain in domainPlatformMap.Keys)
            {
                _siteContexts.TryAdd(domain, domainPlatformMap[domain]);
            }
        }

        private void PopulateFromConfiguration()
        {
            var domainOptions = _configuration.GetSection("DomainOptions").Get<Dictionary<Platform, string[]>>();
            if (domainOptions != null)
            {
                foreach (var platform in domainOptions.Keys)
                {
                    foreach (var domain in domainOptions[platform])
                    {
                        _siteContexts.TryAdd(domain, new SiteContext { Hostname = domain, Platform = platform, SiteName = DefaultSiteName });
                    }
                }
            }
        }
    }
}
