﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Travlr.Shared.Application.Contexts;
using Travlr.Shared.Application.Options;
using Travlr.Shared.Core.Enums;

namespace Travlr.Shared.Web.Options
{
    public class PlatformOptions<T> : IPlatformOptions<T> where T : class
    {
        private readonly IServiceProvider _serviceProvider;

        public PlatformOptions(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public T Value
        {
            get
            {
                var siteContext = _serviceProvider.GetService<ISiteContext>();
                var platform = siteContext != null ? siteContext.Platform : Platform.Bali;

                return PlatformOptions.GetValue<T>(platform);
            }
        }
    }

    public class DummyPlatformOptions<T> : IPlatformOptions<T> where T : class
    {
        public DummyPlatformOptions(T value)
        {
            Value = value;
        }

        public T Value { get; }
    }

    public static class PlatformOptions
    {
        private const string DefaultPlatformPath = "Default";
        private static ConcurrentDictionary<Type, IDictionary<Platform, object>> _optionDefinitions;

        public static void ConfigurePlatformOptions<T>(this IServiceCollection services, string sectionPath, IConfiguration configuration, string relativePath = "") where T : class
        {
            var serviceProvider = services.BuildServiceProvider();
            if (_optionDefinitions == null)
            {
                _optionDefinitions = new ConcurrentDictionary<Type, IDictionary<Platform, object>>();
            }

            var defaultConfig = configuration.Get<T>(sectionPath, DefaultPlatformPath, relativePath);

            var dict = Enum.GetValues(typeof(Platform))
                .Cast<Platform>()
                .ToDictionary(
                    x => x,
                    x => (object)configuration.GetPlatformConfig(sectionPath, x, serviceProvider, defaultConfig, relativePath)
                );

            _optionDefinitions.TryAdd(typeof(T), dict);
        }

        public static IPlatformOptions<T> Create<T>(T value) where T : class
        {
            return new DummyPlatformOptions<T>(value);
        }

        public static T Get<T>(this IConfiguration configuration, string sectionPath, string platformPath, string relativePath = "", bool shouldLoadDefault = true) where T : class
        {
            var path1 = sectionPath;
            var path2 = sectionPath;

            if (!string.IsNullOrEmpty(platformPath))
            {
                path1 = $"{path1}:{platformPath}";
                path2 = $"{path2}{platformPath}";
            }

            if (!string.IsNullOrEmpty(relativePath))
            {
                path1 = $"{path1}:{relativePath}";
                path2 = $"{path2}:{relativePath}";
            }

            T value = configuration.GetSection(path1).Get<T>();
            if (value != null)
            {
                return value;
            }

            value = configuration.GetSection(path2).Get<T>();
            if (value != null)
            {
                return value;
            }

            if (!string.IsNullOrEmpty(platformPath) && shouldLoadDefault)
            {
                value = configuration.Get<T>(sectionPath, string.Empty, relativePath, false);
            }

            return value;
        }

        private static T GetPlatformConfig<T>(this IConfiguration configuration, string sectionPath, Platform platform, IServiceProvider serviceProvider, T defaultConfig, string relativePath) where T : class
        {
            var platformConfig = configuration.Get<T>(sectionPath, platform.ToString(), relativePath, false);
            if (platformConfig == null)
            {
                return defaultConfig;
            }

            platformConfig.SetDefaultValue(defaultConfig, serviceProvider);

            return platformConfig;
        }

        private static void SetDefaultValue(this object platformConfig, object defaultConfig, IServiceProvider serviceProvider)
        {
            var properties = platformConfig.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (property.PropertyType == typeof(bool))
                {
                    continue;
                }

                if (property.PropertyType == typeof(string))
                {
                    if (string.IsNullOrEmpty((string)property.GetValue(platformConfig)))
                    {
                        property.SetValue(platformConfig, property.GetValue(defaultConfig));
                    }

                    continue;
                }

                if (property.PropertyType == typeof(int))
                {
                    if ((int)property.GetValue(platformConfig) <= 0)
                    {
                        property.SetValue(platformConfig, property.GetValue(defaultConfig));
                    }

                    continue;
                }

                if (property.PropertyType.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IList<>)))
                {
                    if (property.GetValue(platformConfig) == null)
                    {
                        property.SetValue(platformConfig, property.GetValue(defaultConfig));
                    }
                    else
                    {
                        var list1 = property.GetListValue(platformConfig);
                        var list2 = property.GetListValue(defaultConfig);

                        if (!list1.Any())
                        {
                            property.SetValue(platformConfig, list2);
                        }

                        if (!list2.Any())
                        {
                            property.SetValue(platformConfig, list1);
                        }

                        var genericType = property.PropertyType.GetGenericArguments()[0];
                        var listType = typeof(List<>).MakeGenericType(genericType);
                        var result = (IList)Activator.CreateInstance(listType);
                        var union = list1.Union(list2).ToList();
                        foreach (var item in union)
                        {
                            result.Add(item);
                        }

                        property.SetValue(platformConfig, result);
                    }

                    continue;
                }

                if (property.GetValue(platformConfig) == null)
                {
                    property.SetValue(platformConfig, property.GetValue(defaultConfig));
                }
                else
                {
                    var targetObject = property.GetValue(platformConfig);
                    var sourceObject = property.GetValue(defaultConfig);

                    targetObject.SetDefaultValue(sourceObject, serviceProvider);
                }
            }
        }

        private static IEnumerable<object> GetListValue(this PropertyInfo property, object obj)
        {
            var value = property.GetValue(obj);
            if (value == null)
            {
                return Array.Empty<object>();
            }

            return ((IList)value).Cast<object>();
        }

        public static T GetValue<T>(Platform platform) where T : class
        {
            if (_optionDefinitions == null || !_optionDefinitions.ContainsKey(typeof(T)))
            {
                return null;
            }

            return (T)_optionDefinitions[typeof(T)][platform];
        }
    }
}
