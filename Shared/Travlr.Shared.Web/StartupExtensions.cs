﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using Travlr.Shared.Application.Contexts;
using Travlr.Shared.Application.DependencyInjections;
using Travlr.Shared.Application.ExternalServices;
using Travlr.Shared.Application.ExternalServices.SSO;
using Travlr.Shared.Application.Options;
using Travlr.Shared.Application.UseCases;
using Travlr.Shared.Core.Imagery;
using Travlr.Shared.Infrastructure;
using Travlr.Shared.Infrastructure.AppConfigs;
using Travlr.Shared.Infrastructure.ExternalServices;
using Travlr.Shared.Infrastructure.ExternalServices.SSO;
using Travlr.Shared.Infrastructure.Repositories;
using Travlr.Shared.Web.Contexts;
using Travlr.Shared.Web.Options;

namespace Travlr.Shared.Web
{
    public static class StartupExtensions
    {
        public static void AddTravlrModules(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient(typeof(PlatformDbContext<>));
            services.AddSingleton<SiteContextFactory>();
            services.AddScoped(ResolveSiteContext);
            services.AddTransient<UseCase>();
            services.AddTransient<CachedUseCase>();
            services.AddSingleton(AddImagery);
            services.AddTransient<IWebserviceAPI, WebServiceAPI>();
            services.AddTransient<ISsoService, SsoService>();
            services.AddTransient(typeof(IPlatformOptions<>), typeof(PlatformOptions<>));
            services.ConfigurePlatformOptions<ServiceOptions>("ServiceOptions", configuration);

            RegisterRepositoriesAndUseCases(services, configuration);
        }

        public static ISiteContext ResolveSiteContext(this IServiceProvider serviceProvider)
        {
            var siteContextFactory = serviceProvider.GetService<SiteContextFactory>();

            return siteContextFactory.GetInstance();
        }

        private static void RegisterRepositoriesAndUseCases(IServiceCollection services, IConfiguration configuration)
        {
            var types = GetAllRepositoriesAndUsesCases();

            foreach(var type in types)
            {
                if (type.IsRepository())
                {
                    services.AddRepository(type);
                }
                else if (type.IsUseCaseHandler())
                {
                    services.AddUseCaseHandler(type);
                }
                else if (type.IsServiceConfiguration())
                {
                    services.ConfigureServices(configuration, type);
                }
            }
        }

        public static Imagery AddImagery(IServiceProvider service)
        {
            var serviceOptionsAccessor = service.GetService<IPlatformOptions<ServiceOptions>>();

            return new Imagery(new CDNOption
            {
                Bucket = serviceOptionsAccessor.Value?.CDN?.Bucket,
                CDN = serviceOptionsAccessor.Value?.CDN?.CDN
            });
        }

        private static void ConfigureServices(this IServiceCollection services, IConfiguration configuration, Type type)
        {
            var serviceConfiguration = (IServiceConfiguration)Activator.CreateInstance(type);

            serviceConfiguration.ConfigureServices(services, configuration);
        }

        private static void AddUseCaseHandler(this IServiceCollection services, Type type)
        {
            var interfaces = type.GetInterfaces();
            if (interfaces == null || !interfaces.Any())
            {
                return;
            }

            var useCaseInterface = interfaces.Single(x => x.IsUseCaseHandlerInterface());
            services.AddTransient(useCaseInterface, type);
            
            var cacheableUseCaseInterface = interfaces.SingleOrDefault(x => x.IsCacheableUseCaseHandlerInterface());
            if (cacheableUseCaseInterface != null)
            services.AddTransient(cacheableUseCaseInterface, type);
        }

        private static void AddRepository(this IServiceCollection services, Type type)
        {
            services.AddTransientOfClassFromItsInterface(type);
        }

        private static void AddTransientOfClassFromItsInterface(this IServiceCollection services, Type type)
        {
            var interfaces = type.GetInterfaces();
            if (interfaces == null || !interfaces.Any())
            {
                return;
            }

            var repositoryInterface = interfaces.Single(x => x.Name.EndsWith(type.Name));

            services.AddTransient(repositoryInterface, type);
        }

        private static IList<Type> GetAllRepositoriesAndUsesCases()
        {
            return AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes())
                 .Where(x => !x.IsInterface && !x.IsAbstract)
                 .Select(x => x).ToList();
        }

        private static bool IsRepository(this Type x)
        {
            if (!x.Name.EndsWith("Repository"))
            {
                return false;
            }

            return IsRepositoryBase(x.BaseType) || (x.BaseType != null && IsRepositoryBase(x.BaseType.BaseType));
        }

        private static bool IsRepositoryBase(Type x)
        {
            if (x == null || !x.IsAbstract || !x.IsGenericType)
            {
                return false;
            }

            var genericType = x.GetGenericTypeDefinition();
            return genericType == typeof(RepositoryBase<>) || genericType == typeof(SwitchablePlatformDbContextRepositoryBase<>);
        }

        private static bool IsUseCaseHandler(this Type x)
        {
            return x.GetInterfaces().Any(x => x.IsUseCaseHandlerInterface());
        }

        private static bool IsUseCaseHandlerInterface(this Type x)
        {
            return x.IsGenericType 
                && (x.GetGenericTypeDefinition() == typeof(IUseCaseHandler<>) 
                    || x.GetGenericTypeDefinition() == typeof(IUseCaseHandler<,>));
        }

        private static bool IsCacheableUseCaseHandlerInterface(this Type x)
        {
            return x.IsGenericType && x.GetGenericTypeDefinition() == typeof(ICacheableUseCaseHandler<,>);
        }

        private static bool IsServiceConfiguration(this Type type)
        {
            return type.GetInterfaces().Any(x => x == typeof(IServiceConfiguration));
        }
    }
}
