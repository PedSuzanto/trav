using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Travlr.Shared.Core.Enums;

namespace Travlr.Shared.Core.Imagery
{
    public class ImageUpload
    {
        private string _BaseUrl;
        private string _BucketNameBali;
        private string _BucketNameNewzealand;
        private string _BucketNameFiji;
        private string _BucketNameIndonesia;
        private string _BucketNameGlobal;

        public ImageUpload(
            string BaseUrl,
            string BucketNameBali,
            string BucketNameNewzealand,
            string BucketNameFiji,
            string BucketNameIndonesia,
            string BucketNameGlobal
        )
        {
            _BaseUrl = BaseUrl;
            _BucketNameBali = BucketNameBali;
            _BucketNameNewzealand = BucketNameNewzealand;
            _BucketNameFiji = BucketNameFiji;
            _BucketNameIndonesia = BucketNameIndonesia;
            _BucketNameGlobal = BucketNameGlobal;
        }

        public async Task<ImageUploadResponse> uploadImagesAsync(byte[] fileStream,string fileName, Platform platform)
        {
            var imageuploadResponse = new ImageUploadResponse();
            HttpClient client = new HttpClient();

            MultipartFormDataContent multiContent = new MultipartFormDataContent();

            multiContent.Headers.Add("x-bucket-name", GetBucketName(platform));
            multiContent.Add(new ByteArrayContent(fileStream), "file", fileName);
            var response = await client.PostAsync(_BaseUrl, multiContent).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var resultString = await response.Content.ReadAsStringAsync();
                imageuploadResponse = JsonConvert.DeserializeObject<ImageUploadResponse>(resultString);
            }
            else
            {
                imageuploadResponse.Message = response.ReasonPhrase;
            }

            return imageuploadResponse;

        }

        private string GetBucketName(Platform platform)
        {
            switch (platform)
            {
                case Platform.Bali:
                    return _BucketNameBali;
                case Platform.Fiji:
                    return _BucketNameFiji;
                case Platform.Global:
                    return _BucketNameGlobal;
                case Platform.Newzealand:
                    return _BucketNameNewzealand;
                case Platform.Indonesia:
                    return _BucketNameIndonesia;
                default:
                    return _BucketNameBali;
            }
        }

    }


    public class ImageUploadResponse
    {
        public string Message { get; set; }
        public ImageData Data { get; set; }
    }

    public class ImageData
    {
        public string FileName { get; set; }
        public List<ImageProperty> Images { get; set; }
    }

    public class ImageProperty
    {
        public string Size { get; set; }
        public string FileName { get; set; }
        public string Location { get; set; }
    }
}
