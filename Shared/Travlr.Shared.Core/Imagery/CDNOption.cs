namespace Travlr.Shared.Core.Imagery
{
    public class CDNOption
    {
        public string Bucket { get; set; }

        public string CDN { get; set; }
    }
}
