﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Travlr.Shared.Core.Enums;

namespace Travlr.Shared.Core.Imagery
{
    public class Imagery
    {
        private readonly CDNOption _CDNOption;

        public Imagery(CDNOption CDNOption)
        {
            _CDNOption = CDNOption;
        }

        public string CreateUrl(string url, ImagerySize size = ImagerySize.Original, string fallback = null)
        {
            try
            {
                var uri = new Uri(url);

                if (!url.Contains("s3.ap-southeast-2.amazonaws.com") || string.IsNullOrEmpty(uri.Query))
                {
                    return uri.AbsoluteUri;
                }

                var paths = url.Split("/").Where(u => !string.IsNullOrEmpty(u)).ToArray();
                var filename = paths.Last();
                var splitted = filename.Split(".").ToArray();
                (var hash, var extension) = (splitted[0], splitted[1]);
                var sizeExtension = (size == ImagerySize.Original) ? "" : $"-{Enum.GetName(typeof(ImagerySize), size).ToLower()}";

                // replace bucket to CDN url
                url = !string.IsNullOrEmpty(_CDNOption.CDN) ? url.Replace(_CDNOption.Bucket, _CDNOption.CDN) : url;

                return url.Replace($"{filename}", $"{hash}{sizeExtension}.{Regex.Replace(extension, @"\?v=*.$", "")}");
            }
            catch
            {
                return fallback;
            }
        }

        public string CreateLegacyUrl(string path, ImageryType type)
        {
            var fallback = type == ImageryType.Avatar ? "https://static.trvcdn.net/shared/assets/images/avatar-default.jpg" : "https://static.trvcdn.net/shared/assets/images/default-no-image.jpg";

            if (string.IsNullOrEmpty(path))
            {
                return fallback;
            }

            if (path.Contains("http://") || path.Contains("https://"))
            {
                return path;
            }

            if (type == ImageryType.Avatar || type == ImageryType.Event)
            {
                return $"{_CDNOption?.CDN}/uploads/{this._normalizePath(path)}";
            }

            if (type == ImageryType.Place)
            {
                return $"{_CDNOption?.CDN}/uploads/galleries/{this._normalizePath(path)}";
            }

            if (type == ImageryType.Article)
            {
                return $"{_CDNOption?.CDN}/uploads/blogimages/{this._normalizePath(path)}";
            }

            if (type == ImageryType.City)
            {
                return $"{_CDNOption?.CDN}/uploads/cityimages/{this._normalizePath(path)}";
            }

            return $"{_CDNOption?.CDN}/{this._normalizePath(path)}";
        }

        public string SecureImage(string image)
        {
            return image.Replace("http://","https://");
        }

        private string _normalizePath(string path) => HttpUtility.UrlEncode(path).Replace("%2F", "/");
    }
}
