namespace Travlr.Shared.Core.Enums
{
    public enum ImagerySize
    {
        Original,
        Gallery,
        Cover,
        Square,
        Thumb,
        Avatar
    }
}
