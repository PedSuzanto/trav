namespace Travlr.Shared.Core.Enums
{
    public enum ImageryType
    {
        Avatar,
        Event,
        Place,
        Article,
        City
    }
}
