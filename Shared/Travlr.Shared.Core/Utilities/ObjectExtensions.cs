﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace Travlr.Shared.Core.Utilities
{
    public static class ObjectExtensions
    {
        public static string Serialize(this Object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static string SerializeCamelCase(this Object obj)
        {
            DefaultContractResolver contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            };

            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                Formatting = Formatting.Indented
            });
        }

        public static string SerializeSnakeCase(this Object obj)
        {
            DefaultContractResolver contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new SnakeCaseNamingStrategy()
            };

            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                Formatting = Formatting.Indented
            });
        }
    }
}
