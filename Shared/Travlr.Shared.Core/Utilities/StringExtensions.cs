﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Travlr.Shared.Core.Utilities
{
    public static class StringExtensions
    {
        public static T Deserialize<T>(this string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }

        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static bool IsNull(this string str)
        {
            return String.IsNullOrEmpty(str);
        }

        public static decimal ToDecimal(this string str)
        {
            decimal result;
            if (decimal.TryParse(str, out result))
                return result;
            else
                return 0m;
        }

        public static int GetStableHashCode(this string str)
        {
            unchecked
            {
                int hash1 = 5381;
                int hash2 = hash1;

                for (int i = 0; i < str.Length && str[i] != '\0'; i += 2)
                {
                    hash1 = ((hash1 << 5) + hash1) ^ str[i];
                    if (i == str.Length - 1 || str[i + 1] == '\0')
                        break;
                    hash2 = ((hash2 << 5) + hash2) ^ str[i + 1];
                }

                return hash1 + (hash2 * 1566083941);
            }
        }

        public static string HashHmac256(this string message, string secret)
        {
            Encoding encoding = Encoding.UTF8;
            using (HMACSHA256 hmac = new HMACSHA256(encoding.GetBytes(secret)))
            {
                var msg = encoding.GetBytes(message);
                var hash = hmac.ComputeHash(msg);
                return BitConverter.ToString(hash).ToLower().Replace("-", string.Empty);
            }
        }

        // Source: https://docs.microsoft.com/en-us/dotnet/api/system.security.cryptography.md5?view=netframework-4.8
        public static string HashMd5(this string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }

        public static string Plural(this string word, int count)
        {
            return Plurals.Plural(word, count);
        }

        public static string PluralPhrase(this string word, int count)
        {
            return Plurals.PluralPhrase(word, count);
        }

        public static string MultipleReplacer(this string result, Dictionary<string, string> stringDictionary)
        {
            foreach (var item in stringDictionary)
            {
                result = result.Replace(item.Key, item.Value);
            }

            return result;
        }
    }

    public static class Plurals
    {
        static Dictionary<string, string> _pluralTable =
            new Dictionary<string, string>
            {
                {"entry", "entries"},
                {"image", "images"},
                {"view", "views"},
                {"file", "files"},
                {"result", "results"},
                {"word", "words"},
                {"definition", "definitions"},
                {"item", "items"},
                {"megabyte", "megabytes"},
                {"game", "games"},
                {"day", "days"},
                {"night", "nights"}
            };

        public static string Plural(string word, int count)
        {
            // Special-case count of one.
            // ... Otherwise, return the pluralized word.
            if (count == 1)
            {
                return word;
            }
            return _pluralTable[word];
        }

        public static string PluralPhrase(string word, int count)
        {
            // Returns phrase of complete pluralized phrase.
            // .. Such as "3 files".
            string properPlural = Plural(word, count);
            return count.ToString() + " " + properPlural;
        }
    }
}
