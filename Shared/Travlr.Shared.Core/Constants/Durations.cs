﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travlr.Shared.Core.Constants
{
    public static class DurationInSeconds
    {
        public const int Duration1Sec = 1;
        public const int Duration1Min = 60 * Duration1Sec;
        public const int Duration5Mins = 5 * Duration1Min;
        public const int Duration1Hour = 60 * Duration1Min;
        public const int Duration1Day = 24 * Duration1Hour;
    }
}
