﻿namespace Travlr.Shared.Core.Constants
{
    public static class Options
    {
        public const string PlatformIdentifier = "platform";
        public const string WlpIdentifier = "isite";

        public const string FeaturedActivityOptionKey = "activity_featured";
        public const string CurrenciesOptionKey = "currency";
        public const string CacheFeatureOptionsKey = "feature_switch_cache_usecases";
        public const string CacheFeatureRedis = "redis";
        public const string CacheFeatureMySql = "mysql";
        public const string FeaturedTripsOptionKey = "trips_featured";
        public const string FeaturedShortlistsOptionKey = "shortlists_featured";
        public const string FeaturedDealsOptionKey = "deals_featured";
        public const string FeaturedAccommodationOptionKey = "accommodations_featured";
        public const string FeaturedArticlesOptionKey = "articles_featured";
        public const string FraudDetectionOptionKey = "fraud_detection";
    }
}
