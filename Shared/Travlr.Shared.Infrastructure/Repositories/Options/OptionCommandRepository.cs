﻿using Travlr.Shared.Application.Repositories.Options;

namespace Travlr.Shared.Infrastructure.Repositories.Options
{
    public class OptionCommandRepository : OptionQueryRepository, IOptionCommandRepository
    {
        public OptionCommandRepository(PlatformDbContext<AppDbContext> platformDbContext)
            : base(platformDbContext, false)
        {
        }
    }
}
