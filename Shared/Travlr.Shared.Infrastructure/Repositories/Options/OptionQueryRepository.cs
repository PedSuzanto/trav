﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Travlr.Shared.Application.Repositories.Options;
using Travlr.Shared.Domain.Options;

namespace Travlr.Shared.Infrastructure.Repositories.Options
{
    public class OptionQueryRepository : SwitchablePlatformDbContextRepositoryBase<AppDbContext>, IOptionQueryRepository
    {
        public OptionQueryRepository(PlatformDbContext<AppDbContext> platformDbContext) 
            : base(platformDbContext, true)
        {
        }

        protected OptionQueryRepository(PlatformDbContext<AppDbContext> platformDbContext, bool isReadOnly)
            : base(platformDbContext, isReadOnly)
        {
        }

        public async Task<IList<Option>> GetOptions()
        {
            return await AppDbContext.Options.ToListAsync();
        }

        public async Task<Option> GetOption(string key)
        {
            var result = await AppDbContext.Options.FirstOrDefaultAsync(o => o.Name == key);

            return result;
        }
    }
}
