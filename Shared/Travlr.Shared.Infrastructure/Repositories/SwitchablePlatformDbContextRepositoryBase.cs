﻿using Microsoft.EntityFrameworkCore;
using Travlr.Shared.Application.Repositories;
using Travlr.Shared.Core.Enums;

namespace Travlr.Shared.Infrastructure.Repositories
{
    public abstract class SwitchablePlatformDbContextRepositoryBase<TDbContext> : ISwitchablePlatformDbContextRepository where TDbContext : DbContext
    {
        private TDbContext _dbContext;
        private readonly PlatformDbContext<TDbContext> _platformDbContext;
        private readonly bool _isReadOnly;

        protected SwitchablePlatformDbContextRepositoryBase(PlatformDbContext<TDbContext> platformDbContext, bool isReadOnly)
        {
            _platformDbContext = platformDbContext;
            _isReadOnly = isReadOnly;
        }

        protected TDbContext AppDbContext
        {
            get
            {
                return _dbContext ?? (_dbContext = _platformDbContext.GetDbContext(_isReadOnly));
            }
        }

        public void SwitchPlatformDbContext(Platform platform)
        {
            if (_platformDbContext == null)
            {
                return;
            }

            _dbContext = _platformDbContext.GetPlatformDbContext(platform, _isReadOnly);
        }
    }
}
