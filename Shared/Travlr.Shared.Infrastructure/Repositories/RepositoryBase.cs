﻿using Microsoft.EntityFrameworkCore;

namespace Travlr.Shared.Infrastructure.Repositories
{
    public abstract class RepositoryBase<TDbContext> where TDbContext : DbContext
    {
        protected TDbContext AppDbContext;

        protected RepositoryBase(PlatformDbContext<TDbContext> platformDbContext, bool isReadOnly)
        {
            AppDbContext = platformDbContext.GetDbContext(isReadOnly);
        }
    }
}
