﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Travlr.Shared.Application.Repositories.CacheDatas;
using Travlr.Shared.Domain.Caches;

namespace Travlr.Shared.Infrastructure.Repositories.CacheDatas
{
    public class CacheDataQueryRepository : RepositoryBase<AppDbContext>, ICacheDataQueryRepository
    {
        public CacheDataQueryRepository(PlatformDbContext<AppDbContext> platformDbContext)
            : base(platformDbContext, true)
        {
        }

        protected CacheDataQueryRepository(PlatformDbContext<AppDbContext> platformDbContext, bool isReadOnly)
            : base(platformDbContext, isReadOnly)
        {
        }

        public async Task<CacheData> GetCacheData(int hashCode, DateTime executedUtc)
        {
            var cacheData = await AppDbContext.CacheData
                .Where(cd => cd.HashCode == hashCode && cd.ExpiryUtc > executedUtc)
                .FirstOrDefaultAsync();

            return cacheData;
        }

        public async Task<IEnumerable<string>> GetAllCacheDataClassNames()
        {
            var now = DateTime.UtcNow;

            return await AppDbContext.CacheData
                .Where(cd => cd.ExpiryUtc > now)
                .Select(x => x.ClassName)
                .Distinct()
                .ToListAsync();
        }

        public async Task<IEnumerable<CacheData>> SearchCachedData(string className, string keyword)
        {
            var now = DateTime.UtcNow;
            var query = AppDbContext.CacheData
                .Where(cd => cd.ExpiryUtc > now)
                .AsQueryable();

            if (!string.IsNullOrEmpty(className))
            {
                query = query.Where(x => x.ClassName == className);
            }

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(cd => EF.Functions.Like(cd.InputJson, $"%{keyword}%") ||
                        EF.Functions.Like(cd.ClassName, $"%{keyword}%"));
            }

            return await query
                .OrderBy(cd => cd.ClassName)
                .ToListAsync();
        }
    }
}
