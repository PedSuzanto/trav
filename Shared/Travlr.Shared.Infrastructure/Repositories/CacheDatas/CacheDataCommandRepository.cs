﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Travlr.Shared.Application.Repositories.CacheDatas;
using Travlr.Shared.Domain.Caches;

namespace Travlr.Shared.Infrastructure.Repositories.CacheDatas
{
    public class CacheDataCommandRepository : CacheDataQueryRepository, ICacheDataCommandRepository
    {
        public CacheDataCommandRepository(PlatformDbContext<AppDbContext> platformDbContext)
            : base(platformDbContext, false)
        {
        }

        public async Task ClearCacheData(DateTime executedUtc)
        {
            var expiredHashCodes = AppDbContext.CacheData.Where(cde => cde.ExpiryUtc < executedUtc).Select(cde => cde.HashCode);

            AppDbContext.RemoveRange(expiredHashCodes);

            await AppDbContext.SaveChangesAsync();
        }

        public async Task ExpireCaches(IEnumerable<int> hashCodes)
        {
            var caches = await AppDbContext.CacheData
                .Where(cd => hashCodes.Contains(cd.HashCode))
                .ToListAsync();
            var expiredDate = DateTime.UtcNow.AddDays(-1);

            foreach (var cache in caches)
            {
                cache.SetExpiryUtc(expiredDate);
            }

            await AppDbContext.SaveChangesAsync();
        }

        public async Task PutCacheData(CacheData cacheData)
        {
            var existingCacheData = await AppDbContext.CacheData.FindAsync(cacheData.HashCode);
            if (existingCacheData == null)
            {
                AppDbContext.CacheData.Add(cacheData);
            }
            else
            {
                AppDbContext.CacheData.Update(cacheData);
            }

            await AppDbContext.SaveChangesAsync();
        }
    }
}
