﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Travlr.Shared.Application.ExternalServices;
using Travlr.Shared.Application.ExternalServices.SSO;
using Travlr.Shared.Application.Options;
using Travlr.Shared.Application.UseCases.Authentications.ValidateAuthenticationToken;
using Travlr.Shared.Infrastructure.AppConfigs;

namespace Travlr.Shared.Infrastructure.ExternalServices.SSO
{
    public class SsoService : ISsoService
    {
        private readonly SsoOptions _ssoConfig;
        private readonly IWebserviceAPI _webserviceAPI;

        public SsoService(IPlatformOptions<ServiceOptions> serviceOptions, IWebserviceAPI webserviceAPI)
        {
            _ssoConfig = serviceOptions.Value.SSO;
            _webserviceAPI = webserviceAPI;
        }

        public async Task<ValidateAuthenticationTokenOutput> ValidateToken(string token)
        {
            var url = $"{_ssoConfig.InternalApiHost}/api/auth/auth-token/validate/{token}";

            var result = await _webserviceAPI.Get<ValidateAuthenticationTokenOutput>(url, null);

            return result;
        }
    }
}
