﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Travlr.Shared.Application.ExternalServices;

namespace Travlr.Shared.Infrastructure.ExternalServices
{
    public class WebServiceAPI : IWebserviceAPI
    {
        public async Task<TReturn> Get<TReturn>(string url, object request) where TReturn : class
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string paramString = "";
            if (request != null)
            {
                var properties = from p in request.GetType().GetProperties()
                                 where p.GetValue(request, null) != null
                                 select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(request, null).ToString());

                paramString = String.Join("&", properties.ToArray());
            }

            var uri = $"{url}?{paramString}";
            var result = await client.GetStringAsync(uri);
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            var response = JsonConvert.DeserializeObject<TReturn>(result, settings);

            return response;
        }

        public async Task<TReturn> Post<TReturn>(string url, object request, string mediaType = "application/json") where TReturn : class
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var json = JsonConvert.SerializeObject(request);
            var content = new StringContent(json, Encoding.UTF8, mediaType);
            var result = await client.PostAsync(url, content);
            var resultString = await result.Content.ReadAsStringAsync();
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            var response = JsonConvert.DeserializeObject<TReturn>(resultString, settings);

            return response;
        }

        public async Task<TReturn> Put<TReturn>(string url, object request) where TReturn : class
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var json = JsonConvert.SerializeObject(request);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await client.PutAsync(url, content);
            var resultString = await result.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<TReturn>(resultString);

            return response;
        }
    }
}