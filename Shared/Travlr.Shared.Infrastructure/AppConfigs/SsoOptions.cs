﻿namespace Travlr.Shared.Infrastructure.AppConfigs
{
    public class SsoOptions
    {
        public string Host { get; set; }

        public string InternalApiHost { get; set; }
    }
}
