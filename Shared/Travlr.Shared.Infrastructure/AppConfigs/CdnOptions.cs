﻿namespace Travlr.Shared.Infrastructure.AppConfigs
{
    public class CdnOptions
    {
        public string Bucket { get; set; }

        public string CDN { get; set; }
    }
}
