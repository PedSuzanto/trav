﻿namespace Travlr.Shared.Infrastructure.AppConfigs
{
    public class ConnectionStrings
    {
        public DefaultConnections DefaultConnections { get; set; }
        public LogConnections LogConnections { get; set; }
    }

    public class Connection
    {
        public string ReadWrite { get; set; }
        public string ReadOnly { get; set; }
        public string WriteOnly { get; set; }
    }

    public class DefaultConnections : Connection
    {
    }
    public class LogConnections : Connection
    {
    }
}
