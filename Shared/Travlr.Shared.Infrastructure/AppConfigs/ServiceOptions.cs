﻿namespace Travlr.Shared.Infrastructure.AppConfigs
{
    public class ServiceOptions
    {
        public string Redis { get; set; }

        public SsoOptions SSO { get; set; }

        public CdnOptions CDN { get; set; }
    }
}
