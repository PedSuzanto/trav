﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Travlr.Shared.Application.Contexts;
using Travlr.Shared.Core.Enums;
using Travlr.Shared.Infrastructure.AppConfigs;

namespace Travlr.Shared.Infrastructure
{
    public sealed class PlatformDbContext<TDbContext> where TDbContext : DbContext
    {
        private static ConcurrentDictionary<string, string> _connectionStrings;
        private readonly IConfiguration _configuration;
        private readonly IServiceProvider _serviceProvider;
        private readonly IDictionary<Platform, TDbContext> _readOnlyDbContexts;
        private readonly IDictionary<Platform, TDbContext> _readWriteDbContexts;
        private TDbContext _readOnlyDbContext;
        private TDbContext _readWriteDbContext;

        public PlatformDbContext(IConfiguration configuration, IServiceProvider serviceProvider)
        {
            _configuration = configuration;
            _serviceProvider = serviceProvider;
            _readOnlyDbContexts = new Dictionary<Platform, TDbContext>();
            _readWriteDbContexts = new Dictionary<Platform, TDbContext>();
        }

        public TDbContext GetPlatformReadOnlyDbContext(Platform platform)
        {
            return GetPlatformDbContext(platform);
        }

        public TDbContext GetPlatformReadWriteDbContext(Platform platform)
        {
            return GetPlatformDbContext(platform, false);
        }

        public TDbContext GetDefaultReadOnlyDbContext()
        {
            return _readOnlyDbContext ?? (_readOnlyDbContext = GetDbContext());
        }

        public TDbContext GetDefaultReadWriteDbContext()
        {
            return _readWriteDbContext ?? (_readWriteDbContext = GetDbContext(false));
        }

        public TDbContext GetPlatformDbContext(Platform platform, bool isReadOnly = true)
        {
            var dbContextDictionary = isReadOnly ? _readOnlyDbContexts : _readWriteDbContexts;

            if (!dbContextDictionary.ContainsKey(platform))
            {
                TDbContext dbContext = GetDbContext(isReadOnly, platform.ToString());

                dbContextDictionary.Add(platform, dbContext);
            }

            return dbContextDictionary[platform];
        }

        public TDbContext GetDbContext(bool isReadOnly = false, string platform = "")
        {
            var connectionString = GetConnectionString(isReadOnly, platform);

            var dbContextOptionsBuilder = new DbContextOptionsBuilder<TDbContext>();
            dbContextOptionsBuilder.EnableSensitiveDataLogging().UseMySql(connectionString);

            var dbContext = (TDbContext)Activator.CreateInstance(typeof(TDbContext), dbContextOptionsBuilder.Options);

            return dbContext;
        }

        private string GetConnectionString(bool isReadOnly, string platform)
        {
            var moduleName = typeof(TDbContext).FullName.Split('.')[1];
            var key = $"{moduleName}|{platform}|{isReadOnly}";

            if (_connectionStrings == null)
            {
                _connectionStrings = new ConcurrentDictionary<string, string>();
            }

            if (_connectionStrings.ContainsKey(key))
            {
                return _connectionStrings[key];
            }

            var moduleConnectionStrings = GetConnectionStringsConfig(platform, moduleName);
            var connectionString = isReadOnly ?
                moduleConnectionStrings.DefaultConnections.ReadOnly :
                moduleConnectionStrings.DefaultConnections.ReadWrite;

            _connectionStrings.TryAdd(key, connectionString);
            return _connectionStrings[key];
        }

        private ConnectionStrings GetConnectionStringsConfig(string platform, string moduleName)
        {
            if (string.IsNullOrEmpty(platform))
            {
                try
                {
                    var siteContext = _serviceProvider.GetService<ISiteContext>();
                    platform = siteContext.Platform.ToString();
                }
                catch(InvalidOperationException)
                {
                }
            }

            var infrastructureOptionsSectionKey = $"InfrastructureOptions{platform}";
            if (_configuration.GetChildren().All(item => item.Key != infrastructureOptionsSectionKey))
            {
                infrastructureOptionsSectionKey = "InfrastructureOptions";
            }

            var infrastructureOptionsSection = _configuration.GetSection(infrastructureOptionsSectionKey);
            var moduleConnectionStringsSectionKey = $"{moduleName}ConnectionStrings";
            if (infrastructureOptionsSection.GetChildren().All(item => item.Key != moduleConnectionStringsSectionKey))
            {
                moduleConnectionStringsSectionKey = "ConnectionStrings";
            }

            var moduleConnectionStrings = infrastructureOptionsSection.GetSection(moduleConnectionStringsSectionKey).Get<ConnectionStrings>();
            return moduleConnectionStrings;
        }
    }
}
