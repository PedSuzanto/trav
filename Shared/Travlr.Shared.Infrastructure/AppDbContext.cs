﻿using Microsoft.EntityFrameworkCore;
using Travlr.Shared.Domain.Caches;
using Travlr.Shared.Domain.Options;

namespace Travlr.Shared.Infrastructure
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AppDbContext).Assembly);
        }

        public DbSet<Option> Options { get; set; }
        public DbSet<CacheData> CacheData { get; set; }
    }
}
