﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Travlr.Shared.Domain.Caches;

namespace Travlr.Shared.Infrastructure.Configurations
{
    public class CacheDataConfiguration : IEntityTypeConfiguration<CacheData>
    {
        public void Configure(EntityTypeBuilder<CacheData> builder)
        {
            builder.ToTable("cache_data")
                .HasKey(x => x.HashCode);
        }
    }
}
