﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Travlr.Shared.Domain.Options;

namespace Travlr.Shared.Infrastructure.Configurations
{
    public class OptionConfiguration : IEntityTypeConfiguration<Option>
    {
        public void Configure(EntityTypeBuilder<Option> builder)
        {
            builder.ToTable("options");
            builder.HasKey(x => x.Id);
            builder.Ignore(x => x.IsPlatformOption)
                .Ignore(x => x.IsWhiteLabelProductOption);
            builder.Property(x => x.CreatedAt).HasColumnName("created_at");
            builder.Property(x => x.UpdatedAt).HasColumnName("updated_at");
        }
    }
}
